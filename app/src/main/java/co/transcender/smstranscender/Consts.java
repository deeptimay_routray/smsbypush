package co.transcender.smstranscender;

import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;

/**
 * Class containing application wide constants.
 */

public interface Consts {

    String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    String SINCH_APPLICATION_KEY = "39079771-302d-4f35-bc6c-352e441be495";//"bed20400-3b45-4d7d-b066-2fbe8bc4c2c5";


    String NEW_RELIC_KEY = "AA52ef1a6c38e870d53624d4e68ec574a31c3404fb";//"AA5acba41d7cbd3681f4bd41090af6ac19a34f36bd";

//    String MIXPANEL_TOKEN = "7b8f5eb0be8e5456b58ca78be92c5790";

    String TOKEN_TAG = "token";

//    String CACHED_JSON_FILE = "logs_cache.json";

//    String OLD_LOGS = "old_logs";

//    String NEW_LOGS = "new_logs";

    long ALARM_IN_HALF_HOUR = 180000;

    long RECORDING_SYNC_THRESHOLD = 600000;

    //    String DEFAULT_COUNTRY_CODE = "+91";
    int LOGS_PAGE_SIZE = 20;
    //    int HTTP_TIME_OUT = 30000;
    int WAIT_FOR_SMS_TIME_MILLI = 180000;
    int API_PROCESS_TIME_MILLI = 10000000;

    // Database Version
    int DATABASE_VERSION = 15;
    //	String appURL = "http://stageapi.voicetree.info";// Test URL
//    String appURL = "http://newstageapi.voicetree.info";// Test URL
//	String appURL = "http://testapi.voicetree.info";// Test URL

    //        String appURL = "https://api.myoperator.co";// Live URL
    String appURL = "http://newapi.myoperator.co";// New Live URL
//    String appURL = "http://microapi.myoperator.co";// New test URL

    String REMOVE_USERS_URL = appURL + "/removenumbers";
    // String CALL_RECORDING_SYNC_URL = appURL + "/upload/voice";//"http://newapi.myoperator.co/upload/voice"
    String CALL_RECORDING_SYNC_URL = appURL + "/upload/voice_amr";

    String Dashboard_URL = appURL + "/dashboard/mobile";
    String DASHBOARD_GRAPH_URL = appURL + "/dashboard/hourly_call_report";
//    String DASHBOARD_URL = appURL + "/dashboard_data";//old//Deprecated

    // Compressed Url
    String GET_USERS_COMPRESSED_URL = appURL + "/viewnumbers/compress";
//    String GET_ONLY_USERS_COMPRESSED_URL = appURL + "/view/users";

    //Followup Urls
//    String GET_FOLLOW_UP_URL = appURL + "/followups";
//    String MARK_DONE_URL = appURL + "/mark_done";
//    String SNOOZE_DONE_URL = appURL + "/snooze";
//    String RESNOOZE_DONE_URL = appURL + "/resnooze";

    //profiles
    String GET_USER_PROFILE_URL = appURL + "/profile/get";
    String SET_USER_PROFILE_URL = appURL + "/profile/edit";

    String FETCH_CALLER_DATA = appURL + "/v2/fetch_caller_number";
    String FETCH_CALLER_ADD_NOTE = appURL + "/add_comment/live";
    String CALL_TRANSFER_URL = appURL + "/transfer_mobile";

    String SET_OTHER_USER_PROFILE_URL = appURL + "/edituser";
    //    pri number fetch
    String GET_ALL_PRI_NUMBERS = appURL + "/fetch_pri_numbers";
//    String GET_ONLY_USERS_COMPRESSED_URL = appURL + "/view/users";

    //	String OTP_REQUEST_URL = appURL + "/authenticate"; //Old
//    String OTP_REQUEST_URL = appURL + "/register"; //New
    String REGISTER_USING_SINCH = appURL + "/register/call"; //New Sinch
    String NEW_ACCOUNT_REQUEST_URL = appURL + "/newaccount";
    String DATABASE_URL = appURL + "/download_csv";

    String TERMS_AND_CONDITIONS = "http://account.voicetree.co/GeneralTermsAndConditions";
    String PRIVACY_POLICY = "http://support.myoperator.co/support/solutions/articles/3000039224-privacy-policy";
    //	String VERIFY_OTP_URL = appURL + "/activate"; //Old
    String VERIFY_OTP_URL = appURL + "/verifyotp"; //New
    String FORGET_PASSWORD_URL = appURL + "/forgetpassword";
    String GET_COMPANY_LIST_URL = appURL + "/fetchcompanies";
    //    String LINK_TO_COMPANY_URL = appURL + "/linkcompany";
    //    String ADD_USER_URL = appURL + "/linknumbers";
    String ADD_USER_URL = appURL + "/add/user";//new api
    String RECORDING = appURL + "/recordings";
//    String UPLOAD_VOICE = appURL + "/upload/voice";

    //	String GET_FILTER_LIST_URL = appURL + "/filters/mobile";// Old
    String GET_FILTER_LIST_URL = appURL + "/filters/compress";// New

    String LAST_SYNCED_URL = appURL + "/lastsyncedlog";

    String CREATE_NEW_COMPANY_ACCOUNT = appURL + "/setupcompany";
    //    String PROFILE_CREATE = appURL + "/app/guest_account";
    //Contact
    String ADD_CONTACT = appURL + "/contacts/add";
    String SEARCH_CONTACTS = appURL + "/contacts/search";

    // 	String LOGS_URL = appURL + "/api/app/details/"; //old
// 	String LOGS_URL = appURL + "/app/logs/details/"; //new
//    String LOGS_URL = appURL + "/sync/logs"; //new2
    String LOGS_URL = "http://sync.myoperator.co/queue_logs.php"; //new3

    String GET_COMMENTS = appURL + "/get_comment";
    String POST_COMMENTS = appURL + "/add_comment";

    //    	String SEARCH_URL = appURL + "/search/compress";// Compressed Old
//    String SEARCH_URL_ = appURL + "/search/mobile";// witout comress
    String SEARCH_URL = appURL + "/logs/compress";// Compressed New

    //    String endUserNumber_Url = appURL + "/fetch_caller_number";//String endUserNumber_Url = appURL + "/fcn";//testing
    String logActivity_URL = appURL + "/logactivity";

    String SHARED_PREF = "cached";
    String SHARED_PREF_ADD = "/data/data/co.transcender.smstranscender/shared_prefs/cached.xml";
//	String SHARED_PREF_ADD = "Context.getFilesDir().getPath()/cached.xml";

    String IVR_SUGGESTION_URL = "http://myoperator.co/5-simple-ways-to-make-your-business-number-visible";

    String GCM_Register_URL = appURL + "/register/sns";

    // Database Name
    String DATABASE_NAME = "logsManager.db";

    // Logs tmp table
//    String LOGS_TMP_TABLE = "logs_tmp";

    String COMMENT_TABLE = "comments_table";

    // Logs Name
    String LOGS_TABLE = "logs";
    String TMP_LOGS_TABLE = "tmp_logs";
    //   String LOGS_TABLE = "logs";

    // Reminder
//    String REMINDER_TABLE = "reminder";

    // call_transfer_table*
    String CALL_TRANSFER_TABLE = "call_transfer_table";

    String SEARCH_CALL_TRANSFER_TABLE = "search_call_transfer_table";

    // Lost Logs Name
    // String LOST_TABLE = "lost";

    // Search table
    String SEARCH_TABLE = "search";

    // Search table delete
    int LOG_TABLE_DELETE = 40;

    // Search table
    String FILTER_TABLE = "filter";

    // Follow table
    String FOLLOW_REMINDER_TABLE = "follow_reminder";

    // Guest Account tmp table
//    String GUEST_TMP_TABLE = "guest_logs";

    // Contact tmp table
//    String CONTACTS_TMP_TABLE = "contacts_tmp";

    // Contact table
    String CONTACTS_TABLE = "contacts";

    // Contact detail tmp table
//    String CONTACTS_DETAIL_TMP_TABLE = "contacts_detail_tmp";

    // Contact detail tmp table
    String BLOCK_NUM_TABLE = "block_num";

    // Recording Sync table
    String RECORDED_SYNC_TABLE = "recorded_sync";

    // Contact detail table
    String CONTACTS_DETAIL_TABLE = "contacts_detail";

    // Search suggestion table
//    String SEARCH_SUGGESTION_TABLE = "search_suggestion";

    // Call table
    Uri CALLLOG_PROVIDER = CallLog.Calls.CONTENT_URI;// Uri.parse("content://call_log/calls");
    Uri CONTACTS_PROVIDER = ContactsContract.Contacts.CONTENT_URI;
    // SMS table
    Uri SMS_PROVIDER = Uri.parse("content://sms");

    String IS_POST_GET_SERVICES_START = "StartPostGetServices";

    // Table Fields
    String CALL_TAG = "call";
    String SMS_TAG = "sms";

    String INCOMING_EVENT = "incoming";
    String INCOMING_MISSED = "incoming_missed";
    String INCOMING_VOICEMAIL = "incoming_voicemail";

    String SMS_FAILED = "sms_failed";

//    String MOBILE_SUCCESS_SMS_STATUS = "success";
//    String MOBILE_FAILED_SMS_STATUS = "failed";

    String OUTGOING_EVENT = "outgoing";
    String OUTGOING_VOICEMAIL = "outgoing_voicemail";
    String OUTGOING_MISSED = "outgoing_missed";
    String DEFAULT = "ic_launcher";

    String INBOX = "inbox";
    String OUTBOX = "outbox";
    String MISSED_STATUS = "missed";
    String CONNECTED_STATUS = "bridged";
    String VOICEMAIL_STATUS = "voicemail";
    //    String PENDING_STATUS = "pending";
    String SUCCESS_STATUS = "success";
    String FAILED_STATUS = "failed";
    //    String PROCESSING_STATUS = "processing";
    String SAME_USER = "same_user";
    String OTHER_USER = "other_user";

//    int INCOMING_CALL = 1;
//    int OUTGOING_CALL = 2;
//    int INCOMING_MISSED_CALL = 3;
//
//    int INBOX_MSZ = 1;
//    int OUTBOX_MSZ = 2;

    int PHONE_NUMBER_LENGTH = 7;

    String MY_PHONE_NUMBER = "MyPhoneNumber";
    String MY_NAME = "MyName";
    String MY_EMAIL = "MyEmail";
    String COUNTRY_NAME = "CountryName";
    String ACTIVE_FETCH_CALLER = "active_fetch_caller";
    String ACTIVE_FETCH_CALLER_NOTE = "active_fetch_caller_note";
    String DEVICE_ID = "DeviceId";
    String TIMEZONE = "TimeZone";
    String COUNTRY_CODE = "CountryCode";
    String LINKED_COMPANY_UUID = "LinkedCompanyUUID";
    String LINKED_COMPANY_NAME = "LinkedCompanyName";

    // String FILTER_STR = "FilterStr";
//    String FILTER_TYPE = "FilterType";
//    String FILTER_DEFAULT = "default";
//    String FILTER_CUSTOM = "custom";

    String SYNCED_VIEW_TYPE = "SyncedViewType";
    String SYNCED_VIEW_NOTIFY = "SyncedViewNotify";
    //    String REQUEST_VIEW_TYPE = "RequestViewType";
    String COMPANY_SOURCE_TYPE = "requestingSource";
    String MY_ROLE = "myRole";
    String COMPANY_ID = "company_id";
    String DISPLAY_NUMBER = "display_number";
    String ACCOUNT_TYPE = "account_type";
    String PACKAGE_TYPE = "package_type";
//    String SYNC_RECORD = "sync_call_records";

    String SOURCE_MOBILE = "mobile";
    String SOURCE_IVR = "ivr";

    String REMINDER_ENTRY = "reminder";
    String FOLLOWUP_ENTRY = "followup";

    String COMPOSITE_VIEWTYPE = "composite";
    String CORPORATE_VIEWTYPE = "corporate";
    String PERSONAL_VIEWTYPE = "personal";

    String PREVIOUS_ACT = "link_type";
    String WELCOME_ACCOUNT = "welcomeLink";
    String SETTING_ACCOUNT = "settingLink";

    String IS_NEW_ENTRY = "isNewEntry";

    String TO_DATE = "FromDate";

    int linkAction = 1;
//    int unlinkAction = 2;

//    int enableSyncAction = 3;
//    int disableSyncAction = 4;

    int callSyncAction = 5;
    int callUnsyncAction = 6;

    int smsSyncAction = 7;
    int smsUnsyncAction = 8;

    int followNotificationSyncAction = 9;
    int followNotificationUnsyncAction = 10;

    // String ALL_DATA_TYPE = "allDataViewType";
    String ADD_COMAPNY_DIALOG_TYPE = "dialog_type";

    //    int JOIN_COMAPNY_DIALOG = 1;
    int CREATE_NOUSER_DIALOG = 2;
//    int CREATE_USER_DIALOG = 3;
//    int CREATE_SKIP_DIALOG = 4;

//    String DEFAULT_COUNTRY = "India";

    double NOTIFICATION_MAX_HOUR = .1;
    double NOTIFICATION_MIN_HOUR = .1;
    long HOUR_TO_MILLISECONDS = 3600000;

    // dashboard variable ST

    String DashboardWiseJSONPref = "DASHBOARDJSONPREF";
    String DashboardGRAPHJSONPref = "DASHBOARDGRAPHJSONPREF";
    String department_status = "status";
    //    String department_today_calls = "today_calls";
    String department_today_dept_calls = "today_dept_calls";

    String graph_data = "data";


    String user_today_calls = "user_data";
    String caller_today_calls = "caller_data";

    String total_subscribers = "total_subscribers";
    String total_subscribers_today = "total_subscribers_today";


    String department_today_tc = "_tc";
    String department_today_cc = "_cc";

    String user_today_ot = "_ot";
    String user_today_oc = "_oc";
    String user_today_in = "_in";
    String user_today_ic = "_ic";

    String user_today_nu = "_nu";
    String user_today_ru = "_ru";
//    String user_today_tu = "_tu";

    String user_today_na = "_na";

    ///////user list
    String USERLISTPref = "UERLISTJSONPREF";

    //pri numbers
    String PRILISTPref = "PRILISTPref";

    //// UserOTP verfy msg
    String OTPVERIFYMSG = "Your MyOperator authentication code";

//    String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
//    String REGISTRATION_COMPLETE = "registrationComplete";

}
