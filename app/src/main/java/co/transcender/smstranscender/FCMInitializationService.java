//package co.transcender.smstranscender;
//
//import android.util.Log;
//
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
//
///**
// * Created by Deeptimay on 12/8/2016.
// */
//
//public class FCMInitializationService extends FirebaseInstanceIdService {
//    private static final String TAG = "FCMInitializationServic";
//
//    @Override
//    public void onTokenRefresh() {
//        String fcmToken = FirebaseInstanceId.getInstance().getToken();
//
//        Log.d(TAG, "FCM Device Token:" + fcmToken);
//        //Save or send FCM registration token
//    }
//}