package co.transcender.smstranscender;

import android.content.Context;

public class GetPrefrences {

	public static String getUUID(Context context) {
		return Utilities.getSharedPref(context, Consts.LINKED_COMPANY_UUID, "");
	}

	public static String getComapnyName(Context context) {
		return Utilities.getSharedPref(context, Consts.LINKED_COMPANY_NAME, "");
	}

	public static String getComapnyMyRole(Context context) {
		return Utilities.getSharedPref(context, Consts.MY_ROLE, "");
	}

	static String getAccountType(Context context) {
		return Utilities.getSharedPref(context, Consts.ACCOUNT_TYPE, "");
	}

	public static String getSyncViewType(Context context) {
		return Utilities.getSharedPref(context,
				Consts.SYNCED_VIEW_TYPE, Consts.PERSONAL_VIEWTYPE);
	}

	// public static String getRequestViewType(Context context) {
	// String requestViewType = Utilities.getSharedPref(context,
	// REQUEST_VIEW_TYPE, PERSONAL_VIEWTYPE);
	// return requestViewType;
	// }

	public static String getToken(Context context) {
		return Utilities.getSharedPref(context, Consts.TOKEN_TAG, "");
	}

	public static String getSearchKey(Context context) {
		return Utilities.getSharedPref(context, "search_key", "");
	}

	public static long getToDate(Context context) {
		return Utilities.getSharedPref(context, Consts.TO_DATE,
				System.currentTimeMillis() / 1000);
	}

	static String getIsNewUser(Context context) {
		return Utilities.getSharedPref(context, "isNewUser", "0");
	}

	public static String getPhoneNumber(Context context) {
		return Utilities.getSharedPref(context, Consts.MY_PHONE_NUMBER,
				"");
	}

	public static String getCountryName(Context context) {
		return Utilities.getSharedPref(context, Consts.COUNTRY_NAME, "");
	}

	public static String getCountryCode(Context context) {
		return Utilities.getSharedPref(context, Consts.COUNTRY_CODE, "");
	}

	public static String getUserId(Context context) {
		return Utilities.getSharedPref(context, "userId", "");
	}

	static String getDeviceId(Context context) {
		return Utilities.getSharedPref(context, Consts.DEVICE_ID, "");
	}

	public static String getTimeZone(Context context) {
		return Utilities.getSharedPref(context, Consts.TIMEZONE, "");
	}

	public static boolean isVerificationScreen(Context context) {
		return Utilities.getSharedPref(context, "Verification",
				false);
	}

	public static boolean isBusinessWelcomeScreen(Context context) {
		return Utilities.getSharedPref(context,
				"BusinessWelcome", false);
	}
}