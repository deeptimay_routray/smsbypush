package co.transcender.smstranscender;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Utilities {
    public static Map<Long, PendingIntent> reminderAlarmPendingArr = new HashMap<>();
    public static Toast toast;
    private static int STATUS = 0;

    public static void setFirstTimeValue(Context context, boolean isSyncOn) {
        SharedPreferences syncPreferences = context.getSharedPreferences(
                "isFirstTimeView", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = syncPreferences.edit();
        prefEditor.putBoolean("isFirstTime", isSyncOn);
        prefEditor.apply();
    }

    public static void setSharedPref(Context context, String key, String value) {
        // save token in preference
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "cached", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setSharedPref(Context context, String key, boolean value) {
        // save token in preference
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "cached", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        boolean tst = editor.commit();
    }

    public static void setSharedPref(Context context, String key, long value) {
        // save token in preference
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "cached", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static String getSharedPref(Context context, String key,
                                       String defaultVal) {
        // get token from preference
        File file = new File(Consts.SHARED_PREF_ADD);

        String prefToken = defaultVal;
        try {
            if (file.exists()) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(
                        Consts.SHARED_PREF, 0);
                prefToken = sharedPreferences.getString(key, defaultVal);

            }
        } catch (Exception ignored) {
        }

        return prefToken;
    }

    public static boolean isContain(Context context, String key,
                                    String defaultVal) {
        // get token from preference
        File file = new File(Consts.SHARED_PREF_ADD);

        boolean prefToken = false;
        try {
            if (file.exists()) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(
                        Consts.SHARED_PREF, 0);
                prefToken = sharedPreferences.contains(key);

            }
        } catch (Exception ignored) {
        }

        return prefToken;
    }

    public static long getSharedPref(Context context, String key,
                                     long defaultVal) {
        // get token from preference
        File file = new File(Consts.SHARED_PREF_ADD);

        long prefToken = defaultVal;
        if (file.exists()) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(
                    Consts.SHARED_PREF, 0);
            prefToken = sharedPreferences.getLong(key, defaultVal);
        }
        return prefToken;
    }

    public static boolean getSharedPref(Context context, String key,
                                        boolean defaultVal) {
        // get token from preference
        File file = new File(Consts.SHARED_PREF_ADD);

        boolean prefToken = defaultVal;
        if (file.exists()) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(
                    Consts.SHARED_PREF, 0);
            prefToken = sharedPreferences.getBoolean(key, defaultVal);
        }
        return prefToken;
    }

    public static boolean getDefaultSharedPref(Context context, String key,
                                               boolean defaultVal) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(key, defaultVal);
    }

    public static String getDefaultSharedPref(Context context, String key,
                                              String defaultVal) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, defaultVal);
    }

    public static boolean isMyServiceRunning(Context context,
                                             Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static boolean getRunning(Context ctx, String serviceClass) {

        ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);

        List<RunningServiceInfo> runningProcesses = am.getRunningServices(1000);
        for (RunningServiceInfo processInfo : runningProcesses) {
            if (serviceClass.equals(processInfo.service.getClassName())) {
                return true;
            }

        }

        return false;

    }


    public static void setAlarmForService(Context context, long time,
                                          Class<?> className) {
        Log.i("Alarm Registered", className.toString());
//        Utilities.setSharedPref(context,"is_alram_set", 1 );

        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).set(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + time, PendingIntent.getService(
                        context, 0, new Intent(context, className), 0));
    }


    public static void setAlarm(Context context, long time,
                                PendingIntent pendingIntent) {
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).set(
                AlarmManager.RTC_WAKEUP, time, pendingIntent);
        reminderAlarmPendingArr.put(time / 1000, pendingIntent);
    }

    public static void cancelAlarm(Context context, PendingIntent pendingIntent) {
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                .cancel(pendingIntent);
    }

    public static String getTrimmedNumber(String incomingNumber) {
        String number = "";
        if (incomingNumber != null) {
            if (incomingNumber.length() >= Consts.PHONE_NUMBER_LENGTH) {
                number = incomingNumber.substring(incomingNumber.length()
                        - Consts.PHONE_NUMBER_LENGTH);
            } else {
                number = incomingNumber;
            }
        }

        return number;
    }

    public static boolean isRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = activityManager
                .getRunningTasks(Integer.MAX_VALUE);

        for (RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(
                    task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }


    public static boolean isActivityRunning(Context ctx, Class<?> activityClass) {
        ActivityManager activityManager = (ActivityManager) ctx
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = activityManager
                .getRunningTasks(Integer.MAX_VALUE);

        for (RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(
                    task.baseActivity.getClassName()))
                return true;
        }

        return false;
    }


    public static void setSyncTypePref(Context context, String view) {
        // Share data as public view
        Utilities.setSharedPref(context, Consts.SYNCED_VIEW_TYPE, view);
    }

    public static String getSyncTypePref(Context context, String view) {
        return Utilities.getSharedPref(context, Consts.SYNCED_VIEW_TYPE, view);
    }

    public static void saveUserRole(Context context, String userRole) {
        Utilities.setSharedPref(context, Consts.MY_ROLE, userRole);
    }

    public static void killToastEvent() {
        try {
            if (toast.getView().isShown())     // true if visible
                toast.cancel();
        } catch (Exception e) {         // invisible if exception

        }
    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive

        return rand.nextInt((max - min) + 1) + min;
    }


//    private static String pad(int c) {
//        if (c >= 10)
//            return String.valueOf(c);
//        else
//            return "0" + String.valueOf(c);
//    }


    public static void appendLog(String text, Boolean isTimeFlag) {

        String time = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date()).toString();
        int r = (new Random()).nextInt(10) + 1;
        if (r % 3 == 0 && isTimeFlag)
            text = "\n" + getDeviceName() + " " + time + "\n" + text;
        else if (!isTimeFlag)
            text = android.text.format.DateFormat.format("hh:mm:ss", new Date()).toString() + " " + text;
        else
            text = "\n" + android.text.format.DateFormat.format("hh:mm:ss", new Date()).toString() + "\n" + text;
        File logFile;
        int maxFileSize = 1024 * 1024 * 5;


        File newDir = new File("/sdcard/MO");
        //////////// check if directory is avilable or not
        if (!newDir.exists()) {
            try {
                newDir.mkdirs();
            } catch (Exception e) {
                Log.e("Log File ", " " + e.getMessage());

                return;
            }
        } else {
            if (!newDir.canWrite()) {
                Log.e("Log File ", "No permission");

                return;
            }
        }

        String[] dlist = newDir.list();

        if (dlist != null && dlist.length > 0) {
            ///////// create multiple file start
            logFile = getLatestFilefromDir("/sdcard/MO");
            if (logFile != null && logFile.length() > maxFileSize) {
                int newChange = 0;
                String newFile = logFile.getName();
                newFile = newFile.substring(newFile.lastIndexOf("/") + 1, newFile.lastIndexOf("."));
                if (!newFile.equals("log"))
                    newChange = Integer.parseInt(newFile);
                newChange++;
                logFile = new File("/sdcard/MO/" + newChange + ".file");
            }
            //////// create multiple file end

        } else {
            logFile = new File("/sdcard/MO/1.file");
            if (!logFile.exists()) {
                try {
                    logFile.createNewFile();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    Log.e("Log File ", e.getMessage());
                    e.printStackTrace();
                }
            }

        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            assert logFile != null;
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static File getLatestFilefromDir(String dirPath) {
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }


    /**
     * Returns the consumer friendly device name
     */
    private static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }
}
