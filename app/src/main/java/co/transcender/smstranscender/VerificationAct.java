package co.transcender.smstranscender;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class VerificationAct extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String[] PERMISSIONS = {android.Manifest.permission.INTERNET,
            android.Manifest.permission.READ_SMS,
            android.Manifest.permission.RECEIVE_SMS,
            android.Manifest.permission.READ_PHONE_STATE,
            android.Manifest.permission.READ_CALL_LOG,
            android.Manifest.permission.CALL_PHONE,
            android.Manifest.permission.ACCESS_NETWORK_STATE};
    public static Context context;
    public static RelativeLayout verificationProgressLayout;
    private static RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
    Button verifyBtn;
    ArrayList<String> countriesArray = new ArrayList<>();
    String[] countries = new String[232];
    HashMap<String, String> countriesMap = new HashMap<>();
    HashMap<String, String> codesMap = new HashMap<>();
    HashMap<String, String> countrycode = new HashMap<>();
    HashMap<String, String> languageMap = new HashMap<>();
    private TextView countryCodeText;
    private EditText phoneNumText;
    private String[] countryCode;
    private String[] countryName;
    private Dialog countryDialog;
    private int countryPosInList;
    private String country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        checkPlayServices();

//        permissionCheck();

        if (!GetPrefrences.isVerificationScreen(context)) {
            setContentView(R.layout.verification_layout);
            setTitle("Welcome to MyOperator SMS");
            verificationProgressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
            countryCodeText = (TextView) findViewById(R.id.countryCode);
            phoneNumText = (EditText) findViewById(R.id.phoneNumber);
            verifyBtn = (Button) findViewById(R.id.verifyBtn);

            countryCodeText.setText("+91");

//            countryCodeText.setOnClickListener(new CountryCodeTextClick());

            phoneNumText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        verifyBtn.performClick();
                        handled = true;
                    }
                    return handled;
                }
            });


        } else {
            Intent intent = new Intent(VerificationAct.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                String TAG = "VerificationAct";
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    protected void onRestart() {
        super.onRestart();
    }

    public void verifyBtnClick(View v) {

        // Fetch Phone number
        String phoneNumber = phoneNumText.getText().toString().trim();

        // Get country code
        Utilities.setSharedPref(context, Consts.COUNTRY_CODE, "+91");

        // Get country name, Phone number, device id, timeZone
        Utilities.setSharedPref(context, Consts.TIMEZONE, TimeZone.getDefault()
                .getID());

        Utilities.setSharedPref(context, Consts.COUNTRY_NAME, country);
        Utilities.setSharedPref(context, Consts.DEVICE_ID, getDeviceId());

        // Set first time view true
        Utilities.setFirstTimeValue(context, true);

        if (phoneNumber.length() >= Consts.PHONE_NUMBER_LENGTH) {
            // Save some details
            Utilities.setSharedPref(context, Consts.MY_PHONE_NUMBER, phoneNumber);
            startVerification();
//            permissionCheck();
        } else
            Ui.setToast(context, "Please enter your number", Toast.LENGTH_SHORT);
    }

    private boolean needPermissionsRationale(List<String> permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("VerificationAct", String.valueOf(requestCode));
        Log.d("VerificationAct", String.valueOf(resultCode));
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 11:
                permissionCheck();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("VerificationAct", String.valueOf(requestCode));
        Log.d("VerificationAct", Arrays.toString(permissions));
        Log.d("VerificationAct", Arrays.toString(grantResults));
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("VerificationAct", "Success");

                } else {
                    permissionCheck();
                    Log.d("VerificationAct", "Failed");
                }
                break;
        }
    }

    private void startVerification() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("devid",
                    Utilities.getSharedPref(context, Consts.DEVICE_ID, ""));
            jsonObject.put("tel", GetPrefrences.getPhoneNumber(context));
            jsonObject.put("country_code",
                    GetPrefrences.getCountryCode(context));

            Log.i("authenticateUser", jsonObject.toString(2));

        } catch (Exception e) {
            e.printStackTrace();
        }

        postData(String.valueOf(jsonObject));

    }

    private boolean permissionCheck() {

        List<String> missingPermissions = getMissingPermissions(PERMISSIONS);

        if (missingPermissions.isEmpty()) {
            return true;
        } else {
            if (needPermissionsRationale(missingPermissions)) {
                Toast.makeText(this, "This application needs permissions to function properly", Toast.LENGTH_LONG)
                        .show();
            }
            ActivityCompat.requestPermissions(this,
                    missingPermissions.toArray(new String[missingPermissions.size()]),
                    1);
        }
        return false;
    }

    private List<String> getMissingPermissions(String[] requiredPermissions) {
        List<String> missingPermissions = new ArrayList<>();
        for (String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), permission)
                    != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        return missingPermissions;
    }

    public void termsConditionClick(View v) {
        // Start terms and conditions
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(Consts.TERMS_AND_CONDITIONS)));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            finish();
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private String getCountryCode() {

        int i = 0;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getResources().getAssets().open("countries.txt")));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] args = line.split(";");
                countries[i] = args[2];
                i++;
//                countriesArray.add(0, args[2]);//
                countriesMap.put(args[2], args[0]);//name & code
                codesMap.put(args[0], args[2]);//code & name
                countrycode.put(args[1], args[0]);//shortname & code

                languageMap.put(args[1], args[2]);

            }
            reader.close();
        } catch (Exception ignored) {

        }

        // Set country code
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            country = telephonyManager.getSimCountryIso().toUpperCase();
        }

        setCountryPos(setCurrPos(country));
        return getValues(country);
    }

    public int getCountryPos() {
        Log.d("VerificationAct", "countryPosInList " + countryPosInList);
        return countryPosInList;
    }

    public void setCountryPos(int countryPosInList) {
        this.countryPosInList = countryPosInList;
    }

    // return device id
    private String getDeviceId() {
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String iemiNumber = "";
        String myAndroidDeviceId = "";
        try {

            iemiNumber = mTelephonyMgr.getDeviceId();

        } catch (Exception ignored) {

        }
        try {

            myAndroidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        } catch (Exception ignored) {

        }
        Log.d("Verification", "iemiNumber " + iemiNumber);
        Log.d("Verification", "myAndroidDeviceId " + myAndroidDeviceId);
        if (iemiNumber != null && iemiNumber.length() != 0)
            return iemiNumber;
        else if (myAndroidDeviceId != null && myAndroidDeviceId.length() != 0)
            return myAndroidDeviceId;
        else
            return "";
    }

    public String getValues(String kiwi) {
        Iterator myVeryOwnIterator = countrycode.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            if (key.equalsIgnoreCase(kiwi)) {
                Log.d("VerificationAct", "key: " + key);
                return countrycode.get(key);
            }
        }
        return "";
    }

    public String setValues(int kiwi) {
        String cont = countries[kiwi];
        Iterator myVeryOwnIterator = countriesMap.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            if (key.equalsIgnoreCase(cont)) {
                return countriesMap.get(key);
            }
        }
        return "";
    }

    public int setCurrPos(String kiwi) {

        String _country = "";
        Iterator myVeryOwnIterator = languageMap.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            if (key.equalsIgnoreCase(kiwi)) {
                _country = languageMap.get(key);
                Log.d("VerificationAct", "country: " + _country);
                break;
            }
        }

        for (int i = 0; i < countries.length; i++) {
            if (countries[i].equalsIgnoreCase(_country)) {
                return i;
            }
        }
        return 0;
    }

    void postData(final String jsonStr) {

        StringRequest sr = new StringRequest(Request.Method.POST, "http://newapi.myoperator.co/register/call", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("HttpRequest", response);

                JSONObject jsonResponse = null;
                try {
                    jsonResponse = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonResponse != null) {
                    String code = "";
                    try {
                        code = jsonResponse.getString("code");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    int statusCode = Integer.valueOf(code);
                    if (statusCode == 200 || statusCode == 201) {
                        Intent intent = new Intent(VerificationAct.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Your number is not registered with MyOperator");
                        alertDialogBuilder
                                .setMessage("Please contact support")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonParams = new HashMap<>();
                jsonParams.put("data", jsonStr);
                Log.d("HttpRequest", jsonStr);
                return jsonParams;
            }
        };

        sr.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(sr);
    }

//    private class CountryCodeTextClick implements OnClickListener {
//
//        @Override
//        public void onClick(View v) {
//            // custom dialog
//            countryDialog = new Dialog(context);
//            countryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            countryDialog.setContentView(R.layout.country_list);
//
//            ListView countryList = (ListView) countryDialog
//                    .findViewById(R.id.countryList);
//
//            // set Adapter
//            countryList.setAdapter(new CountryListAdapter(context, countries));
//
//            countryDialog.show();
//
//        }
//    }

    private class CountryListAdapter extends BaseAdapter {
        String[] countryName;
        private Viewholder viewholder;
        private Context context;

        CountryListAdapter(Context context, String[] countryName) {
            this.context = context;
            this.countryName = countryName;
        }

        @Override
        public int getCount() {
            return countryName.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            if (convertView == null) {
                viewholder = new Viewholder();
                convertView = ((LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.country_list_row, parent, false);
                viewholder.countryName = (RadioButton) convertView
                        .findViewById(R.id.selectCountrybtn);

                convertView.setTag(viewholder);
            } else {
                viewholder = (Viewholder) convertView.getTag();
            }

            viewholder.countryName.setText(countryName[position]);
            if (position == getCountryPos()) {
                viewholder.countryName.setChecked(true);
            } else {
                viewholder.countryName.setChecked(false);
            }
            viewholder.countryName.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    countryCodeText.setText("+" + setValues(position));
                    setCountryPos(position);

                    viewholder.countryName.setChecked(true);
                    countryDialog.dismiss();
                }
            });

            return convertView;
        }

        class Viewholder {
            RadioButton countryName;
        }
    }

}