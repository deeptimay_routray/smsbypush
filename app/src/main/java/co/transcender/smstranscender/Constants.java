package co.transcender.smstranscender;

/**
 * Created by Deeptimay on 12/9/2016.
 */

class Constants {

    static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    static final String REGISTRATION_COMPLETE = "registrationComplete";
    static String prefGCMID = "gcmidpref";

}
