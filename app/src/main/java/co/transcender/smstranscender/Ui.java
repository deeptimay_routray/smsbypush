package co.transcender.smstranscender;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

class Ui {

    private static Dialog dialog;
    private static Toast toast;
    Context context;

    public Ui(Context context) {
        this.context = context;
    }

    static void setToast(Context context, String messageId, int time) {
        if (toast != null)
            toast.cancel();
        toast = Toast.makeText(context, messageId, time);
        toast.show();
    }


    public static void dismissProgressDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public static void addCompanyInfoDialog(Context context, int title,
                                            int message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        // set title
        alertDialogBuilder.setTitle(context.getString(title));
        // set dialog message
        alertDialogBuilder.setMessage(context.getString(message))
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    public void actionBarHomeUP(ActionBar actionBar, int icon, int title) {
        Drawable drawable = context.getResources().getDrawable(icon);
        actionBar.setIcon(drawable);
        String titleStr = context.getResources().getString(title);
        actionBar.setTitle(titleStr);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    public void actionBarHomeUPForCompany(ActionBar actionBar, int icon,
                                          String title) {
        Drawable drawable = context.getResources().getDrawable(icon);
        actionBar.setIcon(drawable);
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    public void actionBar(ActionBar actionBar, int icon, int title) {
        Drawable drawable = context.getResources().getDrawable(icon);
        actionBar.setIcon(drawable);
        String titleStr = context.getResources().getString(title);
        actionBar.setTitle(titleStr);
        actionBar.setHomeButtonEnabled(true);

    }
}