package co.transcender.smstranscender;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Utilities.setSharedPref(MainActivity.this, "Verification", true);

        send = (Button) findViewById(R.id.button);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPlayServices()) {
                    Intent intent = new Intent(MainActivity.this, RegistrationIntentService.class);
                    startService(intent);
                }
            }
        });


//        new GCMRegisterTask(MainActivity.this);
    }

//    public void send(View view) {
//
////        SharedPreferences sharedPref = MainActivity.this.getSharedPreferences("smstranscender", Context.MODE_PRIVATE);
////        String gcm_id = sharedPref.getString(Constants.prefGCMID, "");
////
////        Toast.makeText(getApplicationContext(), gcm_id, Toast.LENGTH_SHORT).show();
////
////        Intent i = new Intent(Intent.ACTION_SEND);
////
////        i.setData(Uri.parse("mailto:"));
////        i.setType("text/plain");
////        i.putExtra(Intent.EXTRA_EMAIL, gcm_id);
////        i.putExtra(Intent.EXTRA_CC, gcm_id);
////        i.putExtra(Intent.EXTRA_SUBJECT, gcm_id);
////        i.putExtra(Intent.EXTRA_TEXT, gcm_id);
//
////        i.setType("message/rfc822");
//////        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"deeptimay.routray@voicetree.co"});
////        i.putExtra(Intent.EXTRA_SUBJECT, gcm_id);
////        i.putExtra(Intent.EXTRA_TEXT, gcm_id);
////        try {
////            getApplicationContext().startActivity(Intent.createChooser(i, "Send mail..."));
////        } catch (android.content.ActivityNotFoundException ignored) {
////        }
//
////        if (checkPlayServices()) {
////            // Start IntentService to register this application with GCM.
////            Intent intent = new Intent(MainActivity.this, RegistrationIntentService.class);
////            startService(intent);
////        }
//
////        Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts(
////                "sms", contact, null));
////        getApplicationContext().startActivity(Intent.createChooser(smsIntent, message));
//
////        ContentValues values = new ContentValues();
////        values.put("address", "7827904095"); // phone number to send
////        values.put("date", System.currentTimeMillis() + "");
////        values.put("read", "1"); // if you want to mark is as unread set to 0
////        values.put("type", "2"); // 2 means sent message
////        values.put("body", "Hi Test");
////
////        Uri uri = Uri.parse("content://sms/");
////        Uri rowUri = getApplicationContext().getContentResolver().insert(uri, values);
//
//
////        SmsManager sms = SmsManager.getDefault();
////        sms.sendTextMessage("9853563664", null, "Hi Test", null, null);
//
//    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}