package co.transcender.smstranscender;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Deeptimay on 12/9/2016.
 */

class GCMRegisterTask {

    private static String gcm_id;

    GCMRegisterTask(Context context, String token) {
        gcm_id = token;
        saveTo_Server();
    }

    private static void saveTo_Server() {
        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();

        StringRequest sr = new StringRequest(Request.Method.POST, "http://newstageapi.voicetree.info/register/internal_sns", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Change Account", response);

//                Toast.makeText(ctx, response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(context, "error " + error.toString(), Toast.LENGTH_LONG).show();

//                NetworkResponse response = error.networkResponse;
//                if (response == null || response.data == null) {
//                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonParams = new HashMap<>();

//                SharedPreferences sharedPref = context.getSharedPreferences("smstranscender", Context.MODE_PRIVATE);
//                String gcm_id = sharedPref.getString(Constants.prefGCMID, "");

//                Toast.makeText(ctx, gcm_id, Toast.LENGTH_SHORT).show();

                JSONObject jsonObject = new JSONObject();
                try {

//                    jsonObject.put("token", GetPrefrences.getToken(context));
//                    jsonObject.put("user_id", GetPrefrences.getUserId(context));

                    jsonObject.put("gcm_id", gcm_id);
                    jsonObject.put("customer_number", "9853563664");
                    // Output the JSON object we're sending to Logcat:
                    Log.i("authenticateUser", jsonObject.toString());

                } catch (Exception ignored) {

                }
                jsonParams.put("data", jsonObject.toString());
//                Toast.makeText(ctx, jsonObject.toString(), Toast.LENGTH_SHORT).show();
                Log.d("GCMRegisterTask", String.valueOf(jsonParams));
                return jsonParams;
            }
        };

        sr.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(sr);
    }

}