package co.transcender.smstranscender;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Deeptimay on 12/9/2016.
 */

public class PushServices extends GcmListenerService {

    private static final String TAG = "MyGcmListener";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {

        String message = "", title = "";
        String contact = "";
        String type = "";
        JSONObject parameters;
        JSONObject js = null;
        try {
            js = new JSONObject(data.get("default").toString());
            Log.d(TAG, String.valueOf(js));
            message = js.getString("message");
            title = js.getString("title");
            type = (String) js.get("type");
            contact = (String) js.get("contact_number");

            sendSMS(message, title, contact);
//            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }


//        Log.d(TAG, "From: " + from);
//        Log.d(TAG, "Message: " + message);
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    public void sendSMS(String message, String title, String contact) {

//        Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts(
//                "sms", contact, null));
//        getApplicationContext().startActivity(Intent.createChooser(smsIntent, message));

//        ContentValues values = new ContentValues();
//        values.put("address", contact); // phone number to send
//        values.put("date", System.currentTimeMillis() + "");
//        values.put("read", "1"); // if you want to mark is as unread set to 0
//        values.put("type", "2"); // 2 means sent message
//        values.put("body", message);
//
//        Uri uri = Uri.parse("content://sms/");
//        Uri rowUri = getApplicationContext().getContentResolver().insert(uri, values);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(contact, null, message, null, null);

    }
}
